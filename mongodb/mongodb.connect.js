const mongoose = require("mongoose");

async function connect() {
    try {
    await mongoose.connect("mongodb+srv://nverino:nvmongodb@cluster0.9asw3.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
    } catch (error) {
        console.error(error);
        console.error("Error connecting to MongoDB");
    }
}

module.exports = { connect };
